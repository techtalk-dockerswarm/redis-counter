package main

import (
	"context"
	"errors"
	"fmt"
	"net/http"
	"os"
	"os/signal"
	"time"

	"github.com/gin-gonic/gin"
	"gitlab.com/techtalk-dockerswarm/redis-counter/cache"
	"gitlab.com/techtalk-dockerswarm/redis-counter/globals"
	"go.uber.org/zap"
)

var rc *cache.RedisCache = cache.SingleRedis()
var hostname string
var log *zap.SugaredLogger = globals.GetLogger()

func main() {
	var err error
	hostname, err = os.Hostname()
	if err != nil {

	}

	serverPort := os.Getenv("PORT")
	if serverPort == "" {
		serverPort = "8080"
	}

	r := gin.Default()
	r.GET("/health", healthHandler)

	if os.Args[1] == "count" {
		r.GET("/", incrementForHostname)
		log.Debug("Running in counter mode")
	} else {
		r.GET("/", getCounters)
		log.Debug("Running in list mode")
	}

	srv := &http.Server{
		Addr:    fmt.Sprintf(":%s", serverPort),
		Handler: r,
	}

	go func() {
		if err := srv.ListenAndServe(); err != nil {
			log.Fatalf("listen: %s", err)
		}
	}()

	// Wait for interrupt signal to gracefully shutdown the server with
	// a timeout of 5 seconds.
	quit := make(chan os.Signal)
	signal.Notify(quit, os.Interrupt)
	<-quit
	log.Info("Shutdown Server ...")

	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()
	if err := srv.Shutdown(ctx); err != nil {
		log.Fatalf("Server Shutdown:", err)
	}

}

func healthHandler(c *gin.Context) {
	if !rc.Connected() {
		c.AbortWithError(500, errors.New("No connection to Redis"))
		return
	}
	if hostname == "" {
		c.AbortWithError(500, errors.New("No hostname set"))
		return
	}

	c.String(http.StatusOK, "Hi there, I am healthy!")
}

func incrementForHostname(c *gin.Context) {
	inc, err := rc.Increment(hostname)
	if err != nil {
		c.Error(err)
		c.JSON(500, map[string]string{"error": err.Error()})
		return
	}

	c.IndentedJSON(200, map[string]int64{hostname: inc})
}

func getCounters(c *gin.Context) {
	r, err := rc.KeysWithValues()
	if err != nil {
		c.Error(err)
		c.JSON(500, map[string]string{"error": err.Error()})
		return
	}
	c.IndentedJSON(200, r)
}
