package globals

import (
	"go.uber.org/zap"
	"os"
	"sync"
)

var log *zap.SugaredLogger
var onceLogger sync.Once
var onceLoggingMode sync.Once
var onceMode sync.Once
var mode string
var loggingMode string

func GetLogger() *zap.SugaredLogger {
	onceLogger.Do(func() {
		var rawLog *zap.Logger
		if GetLoggingMode() == "DEBUG" {
			rawLog, _ = zap.NewDevelopment()
		} else {
			rawLog, _ = zap.NewProduction()
		}
		defer rawLog.Sync() // flushes buffer, if any
		log = rawLog.Sugar()
		log.Infof("Logger initialised in %v", mode)
	})
	return log
}

func GetMode() string {
	onceMode.Do(func() {

		mode = os.Getenv("MODE")
		if mode == "" {
			mode = "DEBUG"
		}
	})
	return mode
}

func GetLoggingMode() string {
	onceLoggingMode.Do(func() {

		loggingMode = GetMode()
		if loggingMode == "" {
			loggingMode = "DEBUG"
		}
		if loggingMode == "DOCKER" {
			loggingMode = "PRODUCTION"
		}
	})
	return loggingMode
}
