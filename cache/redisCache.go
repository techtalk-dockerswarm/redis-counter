package cache

import (
	"fmt"
	"os"
	"sync"
	"time"

	"github.com/go-redis/redis"
	"github.com/pkg/errors"
	"gitlab.com/techtalk-dockerswarm/redis-counter/globals"
	"go.uber.org/zap"
)

var log *zap.SugaredLogger = globals.GetLogger()

type RedisCache struct {
	redisClient         *redis.Client
	connected           bool
	url                 string
	cacheExpirationTime time.Duration
}

func NewRedis() *RedisCache {
	r := &RedisCache{connected: false}
	r.initRedisClient()

	return r
}

var singleRedis *RedisCache
var onceSingleRedis sync.Once

func SingleRedis() *RedisCache {
	onceSingleRedis.Do(func() {
		singleRedis = NewRedis()
	})

	return singleRedis
}

func (c *RedisCache) Connected() bool {
	return c.redisClient != nil && c.connected
}

func (c *RedisCache) initRedisClient() error {
	c.url = os.Getenv("REDIS_URL")
	if c.url == "" {
		c.url = "redis://localhost:6379/1"
	}

	opt, err := redis.ParseURL(c.url)
	if err != nil {
		log.Fatalw("Could not parse redisURL",
			"redisURL", c.url,
			"error", err)
		return errors.New("could not parse redisURL")
	}

	redisClient := redis.NewClient(opt)
	_, err = redisClient.Ping().Result()
	if err != nil {
		log.Errorw("Could not Ping Redis. Disable Caching", "error", err)
		go c.waitForConnectionAndSet(redisClient)
		return nil
	}

	c.connected = true
	c.redisClient = redisClient

	log.Info("Redis client connected")

	return nil
}

func (c *RedisCache) waitForConnectionAndSet(client *redis.Client) {
	t := time.NewTicker(time.Minute)
	log.Info("Starting reconnect logic for redis")
	for range t.C {
		_, err := client.Ping().Result()
		if err != nil {
			log.Errorw("Could not Ping Redis. Caching stays disabled", "error", err)
			continue
		}
		c.redisClient = client
		c.connected = true
		log.Warn("Connected to Redis, if it is unstable it can add latency")
		t.Stop()
		break
	}
}

func (c *RedisCache) Set(key string, value interface{}, expiration time.Duration) error {
	if !c.Connected() {
		return nil
	}

	_, err := c.redisClient.Set(key, value, expiration).Result()

	return err
}

func (c *RedisCache) CachedOrEmpty(key string) string {
	if c.redisClient == nil || !c.connected {
		return ""
	}

	val, err := c.redisClient.Get(key).Result()
	if err == redis.Nil {
		return ""
	} else if err != nil {
		log.Errorw("Error on retrieving cached results",
			"key", key,
			"error", err)
		return ""
	}

	return val
}

func (c *RedisCache) Increment(key string) (int64, error) {
	if !c.connected || c.redisClient == nil {
		return 0, errors.New("not connected")
	}

	return c.redisClient.Incr(key).Result()

}

func (r *RedisCache) KeysWithValues() (map[string]string, error) {
	if !r.connected || r.redisClient == nil {
		return nil, errors.New("not connected")
	}

	var cursor uint64
	var n int64
	var results map[string]string = map[string]string{}
	for {
		var keys []string
		var err error
		keys, cursor, err = r.redisClient.Scan(cursor, "", 100).Result()
		if err != nil {
			log.Errorw("Error while scanning for cache entries", "error", err)

			return nil, errors.Wrap(err, "Error while scanning for cache entries")
		}

		if len(keys) > 0 {

			values, err := r.redisClient.MGet(keys...).Result()
			if err != nil {
				return nil, err
			}

			for i, value := range values {
				results[keys[i]] = fmt.Sprint(value)
			}

			n += int64(len(keys))
		}

		if cursor == 0 {
			break
		}
	}

	return results, nil
}
