FROM golang:alpine AS build-env

# Add namespace here to resolve /vendor dependencies
ENV NAMESPACE gitlab.com/techtalk-dockerswarm/redis-counter
WORKDIR /go/src/$NAMESPACE

ADD . ./
RUN go test -cover -v ./...
RUN CGO_ENABLED=0 GOOS=linux go build -v -ldflags '-w -s'  -a -installsuffix cgo -o /application ./cmd/redis-counter

FROM quay.io/schjan/healthcheck:latest
ENV MODE=DOCKER \
    PORT=8080
COPY --from=build-env /application /
ENTRYPOINT [ "./application" ]